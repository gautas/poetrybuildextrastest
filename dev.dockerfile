FROM python:3.12

# define a user variable
ENV DOCKER_USER devuser

# make the user group
RUN addgroup --gid 1222 devgroup

# create the user
RUN adduser \
  --disabled-password \
  --gecos "" \
  --ingroup devgroup \
  "$DOCKER_USER"

# set the user
USER "$DOCKER_USER"

# install poetry for dependency management via pipx https://python-poetry.org/docs/master/#installation
RUN python3 -m pip install --user pipx
RUN python3 -m pipx ensurepath
ENV PATH="/home/devuser/.local/bin:${PATH}"
RUN pipx install poetry==1.7.1
ENV POETRY_VIRTUALENVS_IN_PROJECT=true
RUN poetry --version