PHONY: devbuild devrun

devbuild:
	docker build -t poetrybuildextrastest -f dev.dockerfile .


devrun:
	docker run \
	-it \
	--rm \
	-d \
	--name poetrybuildextrastest \
	-v ~/dockvol:/dockvol \
	-v ~/.gitconfig:/etc/gitconfig \
	poetrybuildextrastest